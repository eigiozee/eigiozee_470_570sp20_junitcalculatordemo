package edu.uscb.csci470.calculator;

/**
 * Calculator class that implements basic 
 * arithmetic operations as named methods
 * 
 * @author your_username@email.uscb.edu
 * @version 0.1
 */
public class Calculator {
	/**
	 * A method that implements the addition of two ints
	 * @param a
	 * @param b
	 * @return the sum of a and b
	 */
	public int add( int a, int b ) {
		return a + b;
	} // end method add
	
	/**
	 *  A method that implements the subtraction of two ints
	 * @param a
	 * @param b
	 * @return the subtraction of a and b
	 */
	public int subtract( int a, int b ) {
		return a - b;
	} // end method subtract
	
	/** 
	 * A method that implements the multiplication of two ints
	 * @param a
	 * @param b
	 * @return the multiplication of a and b
	 */
	public long multiply( int a, int b ) {
		return a * b;
	} // end method multiply
	
	/**
	 *  A method that implements the division of two ints 
	 * @param a
	 * @param b
	 * @return the division of a and b
	 */
	public int intDivide(int a , int b) {
		int result;
		if (b == 0) {
			throw new IllegalArgumentException(
					"intDivide method: Cannot divide by zero" );
		} else { 
			result = a / b;
		} // end if/else
		return result;
	} // end method divide
	
	/**
	 *  Return the floating point division result of 
	 *  dividing a by b. Note that this method throws 
	 *  an exception when b == 0
	 * @param a the dividend or numerator
	 * @param b the divisor or denominator
	 * @return the floating-point quotient of a and b
	 */
	
	public double divide ( int a, int b ) {
		double result;
		if (b == 0) {
			throw new IllegalArgumentException(
					"divide method: Cannot divide by zero" );
		} else { 
			result = (double)a / (double)b;
		} // end if/else 
		return result;	
	} // end method divide
	
} // end class Calculator
